<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\location\config\model\ConfigEntityFactory;
use people_sdk\location\config\model\repository\ConfigEntitySimpleRepository;



return array(
    // Location configuration entity services
    // ******************************************************************************

    'people_location_config_entity_factory' => [
        'source' => ConfigEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_location_config_entity_simple_repository' => [
        'source' => ConfigEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_requester'],
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);