<?php

use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\location\datetime\model\repository\DateTimeRepository;



return array(
    // Datetime services
    // ******************************************************************************

    'people_datetime_repository' => [
        'source' => DateTimeRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_requester'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);